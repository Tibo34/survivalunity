﻿using UnityEngine;
using  UnityEngine.UI;

public class ScorePlayer : MonoBehaviour
{
    public int score;
    public Text text;

   

    public void SetScore(int point)
    {
        this.score = point;
        text.text = $"Score : {this.score}";
    }
}
