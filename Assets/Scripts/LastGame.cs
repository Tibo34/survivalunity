using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LastGame : MonoBehaviour
{
    public GameObject score;
    public GameObject name;
    

    // Start is called before the first frame update
    void Start()
    {
        var loadPlayerData = SaveSytem.LoadPlayerData();
        score.GetComponent<TMP_Text>().text = loadPlayerData.score.ToString();
        name.GetComponent<TMP_Text>().text = loadPlayerData.name;
    }

  
}
