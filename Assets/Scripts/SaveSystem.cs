using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.Scripts;
using UnityEngine;

public static class SaveSytem 
{
    public static void SavePlayer(PlayerSettings playerSettings)
    {
        BinaryFormatter formatter=new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerSettings.fun";
        FileStream stream=new FileStream(path,FileMode.OpenOrCreate);
        PlayerData player=new PlayerData(playerSettings);
        formatter.Serialize(stream,player);
        stream.Close();
    }
    public static PlayerData LoadPlayerData()
    {
        string path = Application.persistentDataPath + "/playerSettings.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            var deserialize = formatter.Deserialize(stream) as PlayerData;
            return deserialize;
        }
        return null;
    }
    public static void SaveNamePlayer(string name)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerName.fun";
        FileStream stream = new FileStream(path, FileMode.OpenOrCreate);
        formatter.Serialize(stream, name);
        stream.Close();
    }

    public static string LoadNamePlayer()
    {
        string path = Application.persistentDataPath + "/playerName.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            var deserialize = formatter.Deserialize(stream) as string;
            return deserialize;
        }

        return "Jon";
    }


   
}
