﻿using UnityEngine;

public class CollectibleDamage : MonoBehaviour
{
    public int point = -15;
    public int health = -30;

    public AudioSource audioSource;
    //public PlayerController player;

    private void OnTriggerEnter(Collider other)
    {
        print("Trigger enter!" + other.name + " " + other.gameObject.tag);
        if (other.gameObject.tag == "Player")
        {
            /*var player = other.gameObject.GetComponent<PlayerController>();
              player.UpdatePointHeal(health,point);
            print(name);*/
            audioSource.Play();
           
            Destroy(gameObject);
        }
    }
}
