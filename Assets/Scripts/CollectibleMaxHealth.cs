using UnityEngine;

public class CollectibleMaxHealth : MonoBehaviour
{

    public int maxHealth=20;
    private AudioSource audioSource;

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            var player = other.gameObject.GetComponent<PlayerLocomotionController>();
            player.UpdatePointMAxHeal(maxHealth);
            audioSource.Play();
            Destroy(gameObject);
        }
    }
}
