using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour
{

    public HealthBar HealthBar;
    public int healthmax = 1000;
    public int currentHealth;
    public PlayerLocomotionController Player;

    void Awake()
    {
        currentHealth = healthmax;
        HealthBar.SetMaxHeath(healthmax);
    }

    private void TakeDamage(int dam)
    {
        currentHealth += dam;
        HealthBar.SetHealth(currentHealth);
        TowerIsAlive();
    }

    private void TowerIsAlive()
    {
        Debug.Log("Tower Damage : "+currentHealth);
        if (currentHealth <= 0)
        {
            Debug.Log("Tower Dead");
            Player.PlayerSettings.EndGame();
        }
    }

    public void UpdatePointHeal(int i)
    {
       TakeDamage(i);
    }
}
