using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InputField : MonoBehaviour
{
    public GameObject inputField;

    public void SaveName()
    {
        var text = inputField.GetComponent<TMP_InputField>().text;
        SaveSytem.SaveNamePlayer(text);
    }
}
