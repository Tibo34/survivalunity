using UnityEngine;

public class PlayerSettings : MonoBehaviour
{

    public HealthBar HealthBar;

    public ScorePlayer ScorePlayer;

    private GameManager GameManager;

    public string name="Jon";

    public int maxHealht = 100;
    public int currentHealht = 100;
    public int score = 0;

    public void Awake()
    {
        GameManager = FindObjectOfType<GameManager>();
        name=SaveSytem.LoadNamePlayer();
        SetHealthMax(maxHealht);
        SetScore(score);
        DontDestroyOnLoad(this);
    }
    
    public void SetHealth(int point)
    {
        currentHealht += point;
        HealthBar.SetHealth(currentHealht);
        PlayerIsAlive();
    }

    private void PlayerIsAlive()
    {
        if (currentHealht <= 0)
        {
            EndGame();
        }
    }

    public void EndGame()
    {
        Debug.Log("End Game playerSettings");
        GameManager.EndGame(this);
    }

    public void SetHealthMax(int point)
    {
        maxHealht += point;
        HealthBar.SetMaxHeath(maxHealht);
    }

    public void SetScore(int point)
    {
        score += point;
        ScorePlayer.SetScore(score);
    }
}
