using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private bool gameHasEnded = false;
    
    public void EndGame(PlayerSettings player)
    {
        if (!gameHasEnded)
        {
            gameHasEnded = true;
            SaveSytem.SavePlayer(player);
            GameOverMenu(player);
        }

    }

    void GameOverMenu(PlayerSettings player)
    {
        SceneManager.LoadScene("GameOver");
    }
}
