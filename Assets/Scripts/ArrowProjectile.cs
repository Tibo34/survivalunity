﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowProjectile : MonoBehaviour
{
    public float force=5;
    public Rigidbody rigidbody;
    Cinemachine.CinemachineImpulseSource source;
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.centerOfMass = transform.position;
    }

    public void Fire()
    {
        rigidbody.AddForce(transform.forward * (100 * Random.Range(1.3f, 1.7f)), ForceMode.Impulse);
        source = GetComponent<Cinemachine.CinemachineImpulseSource>();

        source.GenerateImpulse(Camera.main.transform.forward);
    }

    public void OnCollisionEnter(Collision collision)
    {
        var collisionGameObject = collision.gameObject;
        if (collisionGameObject.name != "Player")
        {
            rigidbody.isKinematic = true;
            StartCoroutine(Countdown());
            if (collisionGameObject.tag == "Mob")
            {
                var mob = collisionGameObject.GetComponent<EnemyController>();
                mob.TakeDamage(force);
                Destroy(gameObject);
            }
        }
    }

    IEnumerator Countdown()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }


}
