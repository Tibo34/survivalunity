﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class RandomSpanner : MonoBehaviour
{
    public GameObject[] spawners;
    public Transform spawnPos;
    private float timer = 30;
    private float timerSave=30;

    void Awake()
    {
        timerSave = Random.Range(10, 20);
        timer = timerSave;
    }
    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer<0)
        {
            SpawnRandom();
            timer = timerSave;
        }
        
    }

    void SpawnRandom()
    {
        int randomInt = Random.Range(0, spawners.Length);
        Instantiate(spawners[randomInt], spawnPos.position, spawnPos.rotation);
    }

 
}
