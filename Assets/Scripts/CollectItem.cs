﻿using UnityEngine;

public class CollectItem : MonoBehaviour
{
    public int Health = 20;
    private AudioSource audioSource;


    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            var player = other.gameObject.GetComponent<PlayerLocomotionController>();
            player.UpdatePointHeal(Health);
            audioSource.Play();
            Destroy(gameObject);
        }
    }
}
