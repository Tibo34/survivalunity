﻿namespace Assets.Scripts
{
    [System.Serializable]
    public class PlayerData
    {
        public int score;
        public string name;

        public PlayerData(PlayerSettings player)
        {
            score = player.score;
            name = player.name;
        }
    }
}