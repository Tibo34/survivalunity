using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    
    
    public float speed = 1.0f;
    public int health = 10;
    public int point = 10;
    public int damage = 5;
    private Rigidbody rb;
    private Vector3 movement;
    private Animator _animator;
    private Transform playerTransform;
    private Transform towerTransform;

    private GameObject player;
    private GameObject tower;



    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        _animator = this.GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        tower = GameObject.FindGameObjectWithTag("Tower");
        playerTransform = player.transform;
        towerTransform = tower.transform;

    }

    // Update is called once per frame
    void Update()
    {
        var cible = GetVectorCible();
        cible.Normalize();
        movement = cible;
        transform.forward = cible;
    }

    private Vector3 GetVectorCible()
    {
        float distPlayer = Vector3.Distance(transform.position, playerTransform.position);
        float distTower = Vector3.Distance(transform.position, towerTransform.position);
        Vector3 cible;
        if (distPlayer > distTower)
        {
            cible = towerTransform.position - transform.position;
        }
        else
        {
            cible = playerTransform.position - transform.position;
        }
        return cible;
    }

    private void FixedUpdate()
    {
        if (movement != Vector3.zero)
        {
            moveCharacter(movement);
        }
    }



    void moveCharacter(Vector3 direction)
    {
        rb.MovePosition(transform.position+(direction*speed*Time.deltaTime));
        _animator.SetBool("isRun", direction != Vector3.zero);
    }

    private void OnCollisionEnter(Collision other)
    {
        var gameObjectTag = other.gameObject.tag;
        switch (gameObjectTag)
        {
            case "Player":
                var player = other.gameObject.GetComponent<PlayerLocomotionController>();
                _animator.SetBool("isPunch", true);
                player.UpdatePointHeal(-damage);
                Debug.Log(player.PlayerSettings.currentHealht);
                break;
            case "Tower":
                var tower = other.gameObject.GetComponent<TowerController>();
                _animator.SetBool("isPunch", true);
                tower.UpdatePointHeal(-damage);
                break;
            default:
                break;
        }
    }

    public void TakeDamage(float p)
    {
        health -=(int) p;
        if (health <= 0)
        {
            var playerController = player.GetComponent<PlayerLocomotionController>();
            playerController.UpdatePoint(point);
            Destroy(gameObject);
        }

    }


}
