using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadDataPlayer : MonoBehaviour
{

    public GameObject scoreTMP;

    public GameObject nameTMP;
    // Start is called before the first frame update
    void Start()
    {
        PlayerData loadPlayerData = SaveSytem.LoadPlayerData();
        scoreTMP.GetComponent<TMP_Text>().text = loadPlayerData.score.ToString();
        nameTMP.GetComponent<TMP_Text>().text = loadPlayerData.name;
    }

   
}
